package cr.ac.ucr.ecci.cql.mapasucr;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MapsActivity extends AppCompatActivity/*FragmentActivity*/ implements
        OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private GoogleMap mMap;
    private static final int LOCATION_REQUEST_CODE = 101;
    private GoogleApiClient mGoogleApiClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Creamos la instancia de GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }
    @Override
    // Creamos el menu de opciones
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu: this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
// Implementamos las llamadas al menú de opciones
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_1) {
            // Marcador soda UCR
            marcadorSodaUCR();
            return true;
        } else if (id == R.id.action_2) {
            // Mostrar las opciones de Zoom
            mostrarZoom();
            return true;
        } else if (id == R.id.action_3) {
            // Mover la posicion de la camara y hacer Zoom
            posicionCamaraZoom();
            return true;
        } else if (id == R.id.action_4) {
            // Cambiar el tipo de mapa
            MapaSatelite();
            return true;
        } else if (id == R.id.action_5) {
            // Cambiar el tipo de mapa
            MapaHibrido();
            return true;
        } else if (id == R.id.action_6) {
            // Cambiar el tipo de mapa
            MapaNinguno();
            return true;
        } else if (id == R.id.action_7) {
            // Cambiar el tipo de mapa
            MapaNormal();
            return true;
        } else if (id == R.id.action_8) {
            // Cambiar el tipo de mapa
            MapaTierra();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this
     case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to
     install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Solicitar permisos para la localizacion
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // habilitamos los permisos para localizacion
            mMap.setMyLocationEnabled(true);
        } else {
            solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_REQUEST_CODE);
        }

        // Asignamos el evento de clic en el mapa
        mMap.setOnMapClickListener(this);
        // Asignamos el evento de clic largo en el mapa
        mMap.setOnMapLongClickListener(this);
    }
    // Solicitar el persmiso
    protected void solicitarPermiso(String permissionType, int requestCode) {
        ActivityCompat.requestPermissions (this, new String []{permissionType}, requestCode);
    }
    @Override
// Devuelve el resultado de la solicutud de permisos
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull
            int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // se otorga el permiso
                }
            }
        }
    }
    @Override
    public void onMapClick(LatLng latLng) {
    // cuando se hace clic sobre el mapa se muestran las coordenadas
    // nos movemos a la posicion donde hizo clic
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        // mostramos las coordenadas
        Toast.makeText(getApplicationContext(), latLng.toString(),
                Toast.LENGTH_LONG).show();
    }
    @Override
    public void onMapLongClick(LatLng latLng) {
        // Agregamos un marcador cuando el usuario deja presionado un punto en el mapa
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(latLng.toString())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        // Mensaje
        Toast.makeText(getApplicationContext(),
                "Nuevo marcador: " + latLng.toString(), Toast.LENGTH_LONG).show();
    }
    // Marcador soda UCR
    public void marcadorSodaUCR() {
        if (mMap != null) {
            // Agregar un marcador a la Soda de Odontología de la UCR
            LatLng sodaOdontologia = new LatLng(9.938035, -84.051509);
            // Agrego el marcador
            mMap.addMarker(new MarkerOptions().position(sodaOdontologia).title("Soda de Odontología de la UCR"));
            // Muevo el mapa hacia la posición del marcador
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sodaOdontologia));
        }
    }
    // Mostrar las opciones de Zoom
    public void mostrarZoom() {
        UiSettings mapSettings;
        mapSettings = mMap.getUiSettings();
        mapSettings.setZoomControlsEnabled(true);
    }
    // Mover la posicion de la camara y hacer Zoom
    public void posicionCamaraZoom() {
        LatLng sodaOdontologia = new LatLng(9.938035, -84.051509);
        // Mover la camara
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(sodaOdontologia)
                .zoom(15) // zoom level
                .bearing(70) // bearing // direccion de la camara
                .tilt(25) // tilt angle // inclinacion
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
    // Cambiar el tipo de mapa
    public void MapaSatelite() {
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }
    // Cambiar el tipo de mapa
    public void MapaHibrido() {
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }
    // Cambiar el tipo de mapa
    public void MapaNinguno() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
    }
    // Cambiar el tipo de mapa
    public void MapaNormal() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }
    // Cambiar el tipo de mapa
    public void MapaTierra() {
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }
    // obtener la localizacion actual del dispositivo
    private void obtenerLocalizacion() {
// solo si el mapa esta instanciado
        if (mMap == null) {
            return;
        }
// Antes de solicitar la localizacion tenemos que asegurar que tenemos permisos
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location miLocalizacion =
                LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
// Nos posicionamos en el mapa con la localizacion actual
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(miLocalizacion.getLatitude(), miLocalizacion.getLongitude()))
                .zoom(15) // zoom level
                .bearing(70) // bearing // direccion de la camara
                .tilt(25) // tilt angle // inclinacion
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
    @Override
    // Nos conectamos al API
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }
    @Override
    // Nos deconectamos del API
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}